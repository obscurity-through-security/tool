package rng

import (
	crand "crypto/rand"
	mrand "math/rand"
	"sync"
)

type secureSource struct {
	buf []byte
}

func NewSecureSource() *secureSource {
	return &secureSource{
		buf: make([]byte, 8),
	}
}

var _ mrand.Source64 = &secureSource{}

func (*secureSource) Seed(int64) {} // noop
func (s *secureSource) Int63() int64 {
	s.fill()
	s.buf[0] &= 0b01111111
	return 0 +
		int64(s.buf[0])<<(7*8) +
		int64(s.buf[1])<<(6*8) +
		int64(s.buf[2])<<(5*8) +
		int64(s.buf[3])<<(4*8) +
		int64(s.buf[4])<<(3*8) +
		int64(s.buf[5])<<(2*8) +
		int64(s.buf[6])<<(1*8) +
		int64(s.buf[7])<<(0*8)
}

func (s *secureSource) Uint64() uint64 {
	s.fill()
	return 0 +
		uint64(s.buf[0])<<(7*8) +
		uint64(s.buf[1])<<(6*8) +
		uint64(s.buf[2])<<(5*8) +
		uint64(s.buf[3])<<(4*8) +
		uint64(s.buf[4])<<(3*8) +
		uint64(s.buf[5])<<(2*8) +
		uint64(s.buf[6])<<(1*8) +
		uint64(s.buf[7])<<(0*8)
}

func (s *secureSource) fill() {
	if _, err := crand.Read(s.buf); err != nil {
		panic(err)
	}
}

type lockedSource struct {
	s64 mrand.Source64
	mu  sync.Mutex
}

var _ mrand.Source64 = &lockedSource{}

func (s *lockedSource) Seed(i int64) {
	s.mu.Lock()
	s.s64.Seed(i)
	s.mu.Unlock()
} // noop
func (s *lockedSource) Int63() int64 {
	s.mu.Lock()
	r := s.s64.Int63()
	s.mu.Unlock()
	return r
}

func (s *lockedSource) Uint64() uint64 {
	s.mu.Lock()
	r := s.s64.Uint64()
	s.mu.Unlock()
	return r
}

var (
	SecureRand   = mrand.New(&lockedSource{s64: NewSecureSource()})
	InsecureRand = mrand.New(&lockedSource{s64: mrand.NewSource(SecureRand.Int63()).(mrand.Source64)})
)
