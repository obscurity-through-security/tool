package rng

import (
	"math"
	"sort"
)

func Kumaraswamy(a, b float64) float64 {
	// good approx for beta distribution
	x := Float64()
	// unstable near 1,1
	if a == 1 && b == 1 {
		return x
	}
	return a * b * math.Pow(x, a-1) * math.Pow(1-math.Pow(x, a), b-1)
}

var lastNormal = math.NaN()

func Normal() float64 {
	if !math.IsNaN(lastNormal) {
		r := lastNormal
		lastNormal = math.NaN()
		return r
	}
	// box-muller transform
	u := Float64()
	v := Float64()
	a := math.Sqrt(-2 * math.Log(u))
	s, c := math.Sincos(2 * math.Pi * v)
	lastNormal = a * c
	return a * s
}

func Poisson(lambda float64) uint {
	x := uint(0)
	p := math.Exp(-lambda)
	s := p
	u := Float64()
	for u > s {
		x++
		p *= lambda / float64(x)
		s += p
	}
	return x
}

func Beta(a, b float64) float64 {
	ga := Gamma(a)
	gb := Gamma(b)
	return ga / (ga + gb)
}

func Gamma(a float64) float64 {
	// https://sci-hub.ru/10.1007/BF02293108
	// method GT
	i, f := math.Modf(a)
	e := math.E
	gs := func(f float64) float64 {
		if f == 0 {
			return 0
		}
		for {
			u := Float64()
			v := Float64()
			w := Float64()
			var r float64
			var n float64
			if u <= e/(e+f) {
				r = math.Pow(v, 1/f)
				n = w * math.Pow(r, f-1)
			} else {
				r = 1 - math.Log(v)
				n = w * math.Exp(-r)
			}
			if n <= math.Pow(r, f-1)*math.Exp(-r) {
				return r
			}
		}
	}
	gm := func(x float64) float64 {
		n := int(math.Round(x))
		if n == 0 {
			return 0
		}
		i := 1
		p := 1.0
		for {
			u := Float64()
			p *= u
			if i == n {
				return -math.Log(p)
			}
			i++
		}
	}
	return gs(f) + gm(i)
}

func KolmogorovSmirnovTest(a, b []float64) float64 {
	m := float64(len(a))
	n := float64(len(b))

	mn := math.Sqrt((m + n) / (m * n))

	// sort and generate cdf
	sort.Float64s(a)
	sort.Float64s(b)

	cdf := func(arr []float64) map[float64]int {
		last := math.Inf(-1)
		r := map[float64]int{}
		for i := 0; i < len(arr); i++ {
			if arr[i] != last {
				r[arr[i]] = i
				last = arr[i]
			}
			r[arr[i]]++
		}
		return r
	}
	cdfa := cdf(a)
	cdfb := cdf(b)

	// find d
	pa := 0
	pb := 0
	d := 0.0
	update := func() {
		abs := float64(cdfa[a[pa]])/m - float64(cdfb[b[pb]])/n
		if abs < 0 {
			abs = -abs
		}
		if abs > d {
			d = abs
		}
	}
	inlimit := func() bool {
		return pa < len(a) && pb < len(b)
	}
	lesser := func() bool {
		return a[pa] < b[pb]
	}
	for inlimit() {
		for inlimit() && lesser() {
			update()
			pa++
		}
		for inlimit() && !lesser() {
			update()
			pb++
		}
	}

	// convert d to P for ease of use
	ca := d / mn
	lna2 := ca * ca * -2
	p := math.Exp(lna2) * 2
	return p
}
