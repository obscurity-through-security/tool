package rng

import (
	"math"

	"golang.org/x/exp/slices"
)

var Rand = SecureRand

func PutBytes(b []byte) {
	if _, err := Rand.Read(b); err != nil {
		panic(err)
	}
}

func Bytes(n int) []byte {
	b := make([]byte, n)
	PutBytes(b)
	return b
}

func Uint64n(n uint64) uint64 {
	if n&(n-1) == 0 {
		return Uint64() & (n - 1)
	}
	u := Uint64()
	max := (math.MaxUint64 / n) * n
	for u > max {
		u = Uint64()
	}
	return u % n
}

func Int() int                { return Rand.Int() }
func Intn(n int) int          { return Rand.Intn(n) }
func Uint() uint              { return uint(Rand.Uint64()) }
func Uintn(n uint) uint       { return uint(Uint64n(uint64(n))) }
func Int63() int64            { return Rand.Int63() }
func Int63n(n int64) int64    { return Rand.Int63n(n) }
func Int31() int32            { return Rand.Int31() }
func Int31n(n int32) int32    { return Rand.Int31n(n) }
func Int15() int16            { return int16(Rand.Int63() >> 48) }
func Int15n(n int16) int16    { return int16(Uint64n(uint64(n))) }
func Int7() int8              { return int8(Rand.Int63() >> 56) }
func Int7n(n int8) int8       { return int8(Uint64n(uint64(n))) }
func Uint64() uint64          { return Rand.Uint64() }
func Uint32() uint32          { return Rand.Uint32() }
func Uint32n(n uint32) uint32 { return uint32(Uint64n(uint64(n))) }
func Uint16() uint16          { return uint16(Rand.Int63() >> 47) }
func Uint16n(n uint16) uint16 { return uint16(Uint64n(uint64(n))) }
func Uint8() byte             { return byte(Rand.Int63() >> 55) }
func Uint8n(n byte) byte      { return byte(Uint64n(uint64(n))) }
func Float32() float32        { return Rand.Float32() }
func Float64() float64        { return Rand.Float64() }
func Perm(n int) []int        { return Rand.Perm(n) }

func Shuffle[T any](a []T) {
	l := len(a)
	var k T
	for i := l - 1; i > 0; i-- {
		j := Intn(i + 1)
		k = a[i]
		a[i] = a[j]
		a[j] = k
	}
}

func Pick[T any](a []T) T {
	return a[Intn(len(a))]
}

func PickN[T any](a []T, n int) []T {
	r := make([]T, n)
	p := make([]int, n)

	for i := 0; i < n; {
		x := Intn(len(a))
		if slices.Contains(p, x) {
			continue
		}
		p[i] = x
		r[i] = a[x]
		i++
	}
	return r
}
