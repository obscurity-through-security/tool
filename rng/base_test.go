package rng_test

import (
	"testing"

	"codeberg.org/obscurity-through-security/tool/rng"
)

func TestFloat64(t *testing.T) {
	a := getArray(func() float64 {
		return rng.SecureRand.Float64()
	}, 1000000)
	b := getArray(func() float64 {
		return rng.InsecureRand.Float64()
	}, 1000000)
	ks := rng.KolmogorovSmirnovTest(a, b)
	if ks < ksAlpha {
		t.Errorf("P=%f", ks)
	}
}
