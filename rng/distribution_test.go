package rng_test

import (
	"math"
	"math/rand"
	"testing"

	"codeberg.org/obscurity-through-security/tool/rng"
	"gonum.org/v1/gonum/stat/distuv"
)

func init() {
	rng.Rand = rng.InsecureRand
}

func BenchmarkKumaraswamy(b *testing.B) {
	for i := 0; i < b.N; i++ {
		rng.Kumaraswamy(10, 10)
	}
}

func BenchmarkNormal(b *testing.B) {
	for i := 0; i < b.N; i++ {
		rng.Normal()
	}
}

func BenchmarkNormal_MathRand(b *testing.B) {
	for i := 0; i < b.N; i++ {
		rand.NormFloat64()
	}
}

func BenchmarkBeta(b *testing.B) {
	for i := 0; i < b.N; i++ {
		rng.Beta(0.5, 1.5)
	}
}

func BenchmarkPoisson(b *testing.B) {
	for i := 0; i < b.N; i++ {
		rng.Poisson(200.5)
	}
}

func BenchmarkPoisson_Gonum(b *testing.B) {
	g := distuv.Poisson{Lambda: 200.5}

	for i := 0; i < b.N; i++ {
		g.Rand()
	}
}

func BenchmarkGamma_Int(b *testing.B) {
	for i := 0; i < b.N; i++ {
		rng.Gamma(10)
	}
}

func BenchmarkGamma_Float(b *testing.B) {
	for i := 0; i < b.N; i++ {
		rng.Gamma(0.9)
	}
}

func BenchmarkGamma_Int_Gonum(b *testing.B) {
	g := distuv.Gamma{Alpha: 10, Beta: 1}
	for i := 0; i < b.N; i++ {
		g.Rand()
	}
}

func BenchmarkGamma_Float_Gonum(b *testing.B) {
	g := distuv.Gamma{Alpha: 0.9, Beta: 1}
	for i := 0; i < b.N; i++ {
		g.Rand()
	}
}

func TestKolmogorovSmirnovTest(t *testing.T) {
	m := 1000
	a := make([]float64, m)
	n := 2000
	b := make([]float64, n)
	b2 := make([]float64, n)

	for i := 0; i < m; i++ {
		a[i] = rng.Float64()*2 - 1
	}
	for i := 0; i < n; i++ {
		b[i] = rng.Float64()*2 - 1
	}
	for i := 0; i < n; i++ {
		b2[i] = rng.Normal()
	}
	alpha := rng.KolmogorovSmirnovTest(a, b)
	if alpha < 0.05 {
		t.Fail()
	}
	alpha = rng.KolmogorovSmirnovTest(b, b2)
	if alpha > 0.05 {
		t.Fail()
	}
}

func BenchmarkKolmogorovSmirnovTest(t *testing.B) {
	m := 1000
	a := make([]float64, m)
	n := 1000
	b := make([]float64, n)

	for i := 0; i < m; i++ {
		a[i] = rng.Float64()
	}
	for i := 0; i < n; i++ {
		b[i] = rng.Float64()
	}

	t.StartTimer()
	for i := 0; i < t.N; i++ {
		rng.KolmogorovSmirnovTest(a, b)
	}
	t.StopTimer()
}

func BenchmarkKolmogorovSmirnovTest_Size(t *testing.B) {
	m := 1000
	a := make([]float64, m)
	n := t.N
	b := make([]float64, n)

	for i := 0; i < m; i++ {
		a[i] = rng.Float64()
	}
	for i := 0; i < t.N; i++ {
		b[i] = rng.Float64()
	}
	t.StartTimer()
	rng.KolmogorovSmirnovTest(a, b)
	t.StopTimer()
}

func fillArray[T any](a []T, f func() T) {
	for i := 0; i < len(a); i++ {
		a[i] = f()
	}
}

func getArray[T any](f func() T, sz int) []T {
	a := make([]T, sz)
	fillArray(a, f)
	return a
}

const (
	sampleSize = 10000
	ksAlpha    = 0.01
)

func TestNormal(t *testing.T) {
	t.Parallel()
	a := getArray(rng.Normal, sampleSize)
	b := getArray(rand.NormFloat64, sampleSize)
	r := distuv.Normal{Sigma: 1}
	c := getArray(r.Rand, sampleSize)
	if rng.KolmogorovSmirnovTest(a, b) < ksAlpha {
		t.Fail()
	}
	if rng.KolmogorovSmirnovTest(c, a) < ksAlpha {
		t.Fail()
	}
}

func TestRandUint64n(t *testing.T) {
	t.Parallel()
	a := getArray(func() float64 {
		return float64(rng.Uint64n(1000))
	}, 1000000)
	b := getArray(func() float64 {
		return float64(rng.Int63n(1000))
	}, 1000000)
	ks := rng.KolmogorovSmirnovTest(a, b)
	if ks < ksAlpha {
		t.Errorf("P=%f", ks)
	}
}

func TestPoisson(t *testing.T) {
	t.Skipf("K-S test not works well with poisson distribution")
	t.Parallel()
	errorCount := 0
	count := 0
	retry := []float64{}
	run := func(lambda float64) {
		p := distuv.Poisson{Lambda: lambda}

		// a := getArray(func() float64 { return float64(rng.Poisson(lambda)) }, sampleSize)
		// b := getArray(func() float64 { return float64(rng.Poisson(lambda)) }, sampleSize)
		a := getArray(p.Rand, sampleSize)
		b := getArray(p.Rand, sampleSize)

		ks := rng.KolmogorovSmirnovTest(a, b)
		if ks < ksAlpha {
			t.Logf("%f, P=%f", lambda, ks)
			retry = append(retry, lambda)
			errorCount++
			errorCount++
		}
	}
	run(20)
	for i := 0.001; i < 0.01; i += 0.001 {
		run(i)
	}
	for i := 0.01; i < 0.1; i += 0.01 {
		run(i)
	}
	for i := 0.1; i < 10; i += 0.1 {
		run(i)
	}
	for i := 10.0; i < 100; i += 1 {
		run(i)
	}
	maxAllowedError := int(math.Ceil(float64(count) * ksAlpha * 2))
	t.Logf("%d/%d, max allowed %d (P=%f)", errorCount, count, maxAllowedError, ksAlpha)
	if errorCount > int(maxAllowedError) {
		t.Fatalf("too many failed cases")
	}
	for i := 0; i < 5; i++ {
		oldec := errorCount
		t.Logf("retry failed cases")
		for _, v := range retry {
			run(v)
		}
		if errorCount == oldec {
			return
		}
	}
	t.Fail()
}

func TestGamma(t *testing.T) {
	t.Parallel()
	errorCount := 0
	count := 0
	retry := []float64{}
	run := func(alpha float64) {
		p := distuv.Gamma{Alpha: alpha, Beta: 1}

		a := getArray(func() float64 { return float64(rng.Gamma(alpha)) }, sampleSize)
		b := getArray(p.Rand, sampleSize)

		ks := rng.KolmogorovSmirnovTest(a, b)
		if ks < ksAlpha {
			t.Logf("%f, P=%f", alpha, ks)
			retry = append(retry, alpha)
			errorCount++
			errorCount++
		}
		count++
	}
	for i := 0.001; i < 0.01; i += 0.001 {
		run(i)
	}
	for i := 0.01; i < 0.1; i += 0.01 {
		run(i)
	}
	for i := 0.1; i < 10; i += 0.1 {
		run(i)
	}
	for i := 10.0; i < 100; i += 1 {
		run(i)
	}
	run(0.99999999)
	run(0.00000001)
	run(1.00000001)
	run(100.0001)
	run(500)
	maxAllowedError := int(math.Ceil(float64(count) * ksAlpha * 2))
	t.Logf("%d/%d, max allowed %d (P=%f)", errorCount, count, maxAllowedError, ksAlpha)
	if errorCount > int(maxAllowedError) {
		t.Fatalf("too many failed cases")
	}
	for i := 0; i < 5; i++ {
		oldec := errorCount
		t.Logf("retry failed cases")
		for _, v := range retry {
			run(v)
		}
		if errorCount == oldec {
			return
		}
	}
	t.Fail()
}

func TestBeta(t *testing.T) {
	t.Parallel()
	sampleSize := 5000
	errorCount := 0
	count := 0
	type param struct {
		a, b float64
	}
	retry := []param{}
	run := func(alpha, beta float64) {
		p := distuv.Beta{Alpha: alpha, Beta: beta}

		a := getArray(func() float64 { return rng.Beta(alpha, beta) }, sampleSize)
		b := getArray(p.Rand, sampleSize)
		ks := rng.KolmogorovSmirnovTest(a, b)
		if ks < ksAlpha {
			t.Logf("(%f,%f), P=%f", alpha, beta, ks)
			retry = append(retry, param{a: alpha, b: beta})
			errorCount++
		}
		count++
	}
	for i := 0.1; i <= 5.0; i += 0.1 {
		for j := 0.1; j <= 5.0; j += 0.1 {
			run(i, j)
		}
	}
	for i := 0.9; i <= 1.1; i += 0.01 {
		for j := 0.9; j <= 1.1; j += 0.01 {
			run(i, j)
		}
	}
	for i := 0.999; i <= 1.001; i += 0.0001 {
		for j := 0.999; j <= 1.001; j += 0.0001 {
			run(i, j)
		}
	}
	maxAllowedError := int(math.Ceil(float64(count) * ksAlpha * 2))
	t.Logf("%d/%d, max allowed %d (P=%f)", errorCount, count, maxAllowedError, ksAlpha)
	if errorCount > int(maxAllowedError) {
		t.Fatalf("too many failed cases")
	}
	for i := 0; i < 5; i++ {
		oldec := errorCount
		t.Logf("retry failed cases")
		for _, v := range retry {
			run(v.a, v.b)
		}
		if errorCount == oldec {
			return
		}
	}
	t.Fail()
}
