module codeberg.org/obscurity-through-security/tool

go 1.20

require (
	github.com/refraction-networking/utls v1.2.2
	golang.org/x/exp v0.0.0-20230213192124-5e25df0256eb
	golang.org/x/net v0.7.0
	gonum.org/v1/gonum v0.12.0
)

require (
	github.com/andybalholm/brotli v1.0.5 // indirect
	github.com/klauspost/compress v1.15.15 // indirect
	golang.org/x/crypto v0.6.0 // indirect
	golang.org/x/sys v0.5.0 // indirect
)
