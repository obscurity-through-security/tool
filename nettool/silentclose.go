package nettool

import (
	ctls "crypto/tls"
	"errors"
	"fmt"
	"net"
	"net/netip"

	tls "github.com/refraction-networking/utls"

	"golang.org/x/net/ipv4"
	"golang.org/x/net/ipv6"
)

func SetTTL(c net.Conn, ttl int) error {
	if t, ok := c.(*ctls.Conn); ok {
		c = t.NetConn()
	}
	if t, ok := c.(*tls.Conn); ok {
		c = t.NetConn()
	}

	addr, err := netip.ParseAddrPort(c.LocalAddr().String())
	if err != nil {
		return fmt.Errorf("can't determine IP address family: %w", err)
	}
	if addr.Addr().Is4() {
		v4 := ipv4.NewConn(c)
		if v4 == nil {
			return errors.New("can't get ipv4.Conn")
		}
		return v4.SetTTL(ttl)
	} else {
		v6 := ipv6.NewConn(c)
		if v6 == nil {
			return errors.New("can't get ipv4.Conn")
		}
		return v6.SetHopLimit(ttl)
	}
}

func Drain(c net.Conn) {
	b := make([]byte, 2048) // longer than mtu
	for {
		_, e := c.Read(b)
		if e != nil {
			return
		}
	}
}

// todo: more way to close connection
// - iptables drop
// - ss -K ?
// - maybe some weird syscall?
// - drain
// - just do nothing
