package nettool

import (
	"context"
	"io"
	"net"
	"sync"
	"time"
)

func ForwardContext(ctx context.Context, c1, c2 net.Conn, timeout time.Duration) error {
	var err error
	var once sync.Once
	go func() {
		<-ctx.Done()
		if err == nil {
			once.Do(func() {
				err = ctx.Err()
			})
		}
	}()
	var w sync.WaitGroup
	w.Add(2)
	go func() {
		e := forward1Context(ctx, c1, c2, timeout)
		if e != nil && err == nil {
			once.Do(func() {
				err = e
			})
		}
		w.Done()
	}()
	go func() {
		e := forward1Context(ctx, c2, c1, timeout)
		if e != nil && err == nil {
			once.Do(func() {
				err = e
			})
		}
		w.Done()
	}()
	w.Wait()
	return err
}

func forward1Context(ctx context.Context, c1, c2 net.Conn, timeout time.Duration) error {
	b := make([]byte, 4096)
	for {
		updateTimeout(c1, timeout)
		n, err := c1.Read(b)
		if err != nil {
			return err
		}
		updateTimeout(c2, timeout)
		n2, err := c2.Write(b[:n])
		if err != nil {
			return err
		}
		if n2 != n {
			return io.ErrShortWrite
		}

	}
}

func updateTimeout(c net.Conn, t time.Duration) {
	if t == time.Duration(0) {
		return
	}

	_ = c.SetDeadline(time.Now().Add(t))
}
