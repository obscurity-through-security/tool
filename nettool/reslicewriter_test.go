package nettool_test

import (
	"context"
	"io"
	"testing"
	"time"

	"codeberg.org/obscurity-through-security/tool/nettool"
)

type StopWatch struct {
	t0 time.Time
	t  []time.Duration

	Accuracy time.Duration
	Timer    func() time.Time
}

func (s *StopWatch) Start() {
	if s.Timer == nil {
		s.Timer = time.Now
	}
	if s.Accuracy == 0 {
		s.Accuracy = 1 * time.Nanosecond
	}

	s.t0 = s.Timer()
}

func (s *StopWatch) Lap() {
	t1 := s.Timer()
	dt := t1.Sub(s.t0)
	s.t = append(s.t, dt)
}

func (s StopWatch) Get(n int) time.Duration {
	return s.t[n]
}

func (s StopWatch) OnTime(n int, t time.Duration) bool {
	tx := s.Get(n)
	dt := tx - t
	if dt < 0 {
		dt = -dt
	}
	return dt <= s.Accuracy
}

func TestResliceWriter_Idle(t *testing.T) {
	pr, pw := io.Pipe()
	ch := make(chan int, 1)
	rw := nettool.NewResliceWriter(pw, &nettool.ResliceWriterConfig{
		Shaper: nettool.ChannelShaper{Ch: ch},
	})

	dt := 12 * time.Millisecond
	n := 100
	size := 10

	p := make([]byte, 1000)
	rw.Start(context.Background())

	go func() {
		t := time.NewTicker(dt)
		for i := 0; i < n; i++ {
			<-t.C
			ch <- size
		}
		t.Stop()
	}()

	sw := &StopWatch{}
	sw.Start()
	for i := 0; i < n; i++ {
		sw.Lap()
		n, err := pr.Read(p)
		if err != nil {
			t.Fatal(err)
		}
		if n != size {
			t.Fail()
		}
	}
	sw.Accuracy = dt / 5
	for i := 0; i < n; i++ {
		if !sw.OnTime(i, time.Duration(i)*dt) {
			t.Error("not on time", i, sw.Get(i))
		}
	}
}

func TestResliceWriter_Write(t *testing.T) {
	pr, pw := io.Pipe()
	ch := make(chan int, 1)
	rw := nettool.NewResliceWriter(pw, &nettool.ResliceWriterConfig{
		Shaper: nettool.ChannelShaper{Ch: ch},
	})

	dt := 12 * time.Millisecond
	n := 100
	size := 10

	p := make([]byte, 1000)
	rw.Start(context.Background())

	go func() {
		tm := time.NewTicker(dt)
		for i := 0; i < n; i++ {
			<-tm.C
			ch <- size
		}
		tm.Stop()
	}()

	go func() {
		b := make([]byte, 5)
		tm := time.NewTicker(dt)
		for i := 0; i < n; i++ {
			_, err := rw.Write(b)
			if err != nil {
				t.Log(err)
				break
			}
		}
		tm.Stop()
	}()

	sw := &StopWatch{}
	sw.Start()
	for i := 0; i < n; i++ {
		sw.Lap()
		n, err := pr.Read(p)
		if err != nil {
			t.Fatal(err)
		}
		if n != size {
			t.Fail()
		}
	}
	sw.Accuracy = dt / 5
	for i := 0; i < n; i++ {
		if !sw.OnTime(i, time.Duration(i)*dt) {
			t.Error("not on time", i, sw.Get(i))
		}
	}
}

func TestResliceWriter_WriteBlocked(t *testing.T) {
	pr, pw := io.Pipe()
	ch := make(chan int, 1)
	rw := nettool.NewResliceWriter(pw, &nettool.ResliceWriterConfig{
		Shaper:            nettool.ChannelShaper{Ch: ch},
		MaxPendingSegment: 1,
	})

	dt := 12 * time.Millisecond
	n := 100
	size := 10

	p := make([]byte, 1000)
	rw.Start(context.Background())

	go func() {
		tm := time.NewTicker(dt)
		for i := 0; i < n; i++ {
			<-tm.C
			ch <- size
		}
		tm.Stop()
	}()

	go func() {
		b := make([]byte, 5)
		tm := time.NewTicker(dt)
		for i := 0; i < n; i++ {
			_, err := rw.Write(b)
			if err != nil {
				t.Log(err)
				break
			}
		}
		tm.Stop()
	}()

	sw := &StopWatch{}
	sw.Start()
	for i := 0; i < n; i++ {
		sw.Lap()
		n, err := pr.Read(p)
		if err != nil {
			t.Fatal(err)
		}
		if n != size {
			t.Fail()
		}
	}
	sw.Accuracy = dt / 5
	for i := 0; i < n; i++ {
		if !sw.OnTime(i, time.Duration(i)*dt) {
			t.Error("not on time", i, sw.Get(i))
		}
	}
}
