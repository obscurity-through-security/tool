package nettool

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"io"
	"os"
	"sync"

	"codeberg.org/obscurity-through-security/tool/rng"
)

// todo PMTUD?

const (
	ethernetV2MTU = 1500    // rfc894
	ipv4Overhead  = 20      // rfc791
	tcp4Overhead  = 20      // rfc793
	tls13Overhead = 13 + 16 // rfc8446, assume 16 byte AEAD overhead
)

// default MSS, TLS 1.3 over IPv4 network over Ethernet.
const defaultMSS = ethernetV2MTU - ipv4Overhead - tcp4Overhead - tls13Overhead

type Shaper interface {
	MSS(int)
	In(int)  // called for every write request
	Out(int) // called for every actual write
	Pattern(context.Context) <-chan int
}

type defaultShaper struct{ mss int }

func (d *defaultShaper) MSS(i int) {
	d.mss = i
}
func (defaultShaper) In(int)  {}
func (defaultShaper) Out(int) {}
func (d defaultShaper) Pattern(ctx context.Context) <-chan int {
	ch := make(chan int)
	go func() {
		select {
		case <-ctx.Done():
			return
		case ch <- d.mss:
		}
	}()
	return ch
}

type ChannelShaper struct {
	Ch chan int
}

func (ChannelShaper) MSS(int) {}
func (ChannelShaper) In(int)  {}
func (ChannelShaper) Out(int) {}
func (d ChannelShaper) Pattern(ctx context.Context) <-chan int {
	return d.Ch
}

type ResliceWriter struct {
	w *bufio.Writer // writer && output buffer
	p <-chan int
	c ResliceWriterConfig

	buf *bytes.Buffer // main reslice buffer
	bch chan []byte   // sending channel
	wf  []byte        // whole frame
	err error

	bmu      sync.Mutex
	wfmu     sync.Mutex
	wfch     chan struct{}
	wfchance bool
}

var (
	_ io.WriteCloser = &ResliceWriter{}
	_ io.ReaderFrom  = &ResliceWriter{} // to control buffer size by ourself
)

// ResliceWriterConfig is config for ResliceWriter.
type ResliceWriterConfig struct {
	// Control when to send data and how much data to be send, need to be created for every ResliceWriter
	Shaper Shaper
	// Estimated link MSS, used to avoid unnecessary fragmentation and setup Shaper
	//
	// zero value will be replaced with default value
	//
	// default value: 1431 (TLS1.3(AES-GCM/CHACHA20-POLY1305)-TCP-IPv4-Ethernet)
	MSS int

	// Maximum send buffer size in times of MSS before Write blocks
	//
	// WARNING: if set to 0, Write will not block, may panic if underlying io.Writer is slower than incoming data stream.
	MaxPendingSegment int
	// Function used to generate padding, may be nil, crypto/rand based generator will be used.
	PadFunc func(n int) []byte
	// When set to true, will not generate frame with only padding,
	// may leak some timing or size information of original stream,
	// but can save some I/O.
	NoIdleNoise bool

	ForcePaddingWholeWrite bool
}

var defaultResliceWriterConfig = ResliceWriterConfig{
	Shaper: &defaultShaper{},
	MSS:    defaultMSS,

	// 10 MSS so about 14kB data can be assembled before underlying Write complete
	// TCP congestion control (should) have minimal effect on this since we don't wait remote acknowledge
	MaxPendingSegment: 10,
	PadFunc:           rng.Bytes,
	NoIdleNoise:       true,
}

func NewResliceWriter(w io.Writer, pconf *ResliceWriterConfig) *ResliceWriter {
	var conf ResliceWriterConfig
	if pconf == nil {
		conf = defaultResliceWriterConfig
	} else {
		conf = *pconf
	}
	if conf.Shaper == nil {
		conf.Shaper = &defaultShaper{}
	}
	if conf.PadFunc == nil {
		conf.PadFunc = rng.Bytes
	}
	if conf.MSS == 0 {
		conf.MSS = defaultMSS
	}

	return &ResliceWriter{
		w:   bufio.NewWriterSize(w, 2*conf.MSS), // safety measure
		c:   conf,
		buf: &bytes.Buffer{},

		bch:  make(chan []byte, 2*conf.MaxPendingSegment),
		wfch: make(chan struct{}),
	}
}

func (r *ResliceWriter) Start(ctx context.Context) {
	r.c.Shaper.MSS(r.c.MSS)
	r.p = r.c.Shaper.Pattern(ctx)

	go r.writeLoop(ctx)
}

func (r *ResliceWriter) Flush() error {
	if r.err != nil {
		return r.err
	}
	remain := r.buf.Bytes()
	_, r.err = r.w.Write(remain)
	return r.err
}

func (r *ResliceWriter) writeLoop(ctx context.Context) {
	for {
		n := 0
		select {
		case <-ctx.Done():
			return
		case n = <-r.p:
		}

		r.err = r.doWrite(n)
		if r.err != nil {
			return
		}
	}
}

func (r *ResliceWriter) fillBuffer() error {
	done := false
	for !done {
		select {
		case b := <-r.bch:
			if _, err := r.buf.Write(b); err != nil {
				return err
			}
			if r.buf.Len() > r.c.MaxPendingSegment*r.c.MSS {
				done = true
			}
		default:
			done = true
		}
	}
	return nil
}

func (r *ResliceWriter) doWrite(n int) error {
	if n > r.c.MSS {
		n = r.c.MSS
	}
	r.bmu.Lock()
	defer r.bmu.Unlock()
	defer r.w.Flush()

	// write wf
	// it's possible previous doWrite reached EOF without sending wf
	// then some data arrived, insert wf at beginning is still safe
	if r.wfchance && len(r.wf) > 0 {
		r.wfchance = false
		orig := n
		n -= len(r.wf)
		_, err := r.w.Write(r.wf)
		r.wf = nil
		r.wfch <- struct{}{}
		if err != nil {
			return err
		}

		if n <= 0 {
			if r.c.ForcePaddingWholeWrite {
				space := r.c.MSS - orig
				if space < 0 {
					panic("whole frame exceed MSS")
				}
				n = rng.Intn(space)
			} else {
				r.wfchance = true
				return nil
			}
		}
	}

	// pull from pending channel
	if r.c.MaxPendingSegment != 0 {
		if err := r.fillBuffer(); err != nil {
			return err
		}
	}
	// skip frame
	if r.buf.Len() == 0 && r.c.NoIdleNoise {
		return nil
	}

	// write buffer
	nb := r.buf.Len()
	if nb > n {
		// big buffer, write as much as possible
		_, err := io.CopyN(r.w, r.buf, int64(n))
		return err
	}
	if nb != 0 {
		// small buffer, padding
		if _, err := io.Copy(r.w, r.buf); err != nil {
			return err
		}
		n -= nb
	}

	if r.buf.Len() == 0 {
		r.wfchance = true

		// append whole frame
		nw := len(r.wf)
		if nw < n && nw > 0 {
			_, err := r.w.Write(r.wf)
			r.wf = nil
			r.wfch <- struct{}{}
			if err != nil {
				return err
			}
		}
	}
	// write padding
	npad := n - nb
	if npad <= 0 {
		return nil
	}
	pad := r.c.PadFunc(npad)
	if pad != nil {
		_, err := r.w.Write(pad)
		return err
	}
	return nil
}

// Write write b to buffer for sending.
// TODO: blocking version to pass back pressure without blow up r.buf
func (r *ResliceWriter) Write(b []byte) (int, error) {
	if r.err != nil {
		return 0, r.err
	}

	if r.c.MaxPendingSegment != 0 {
		r.bch <- b
		return len(b), nil
	}

	r.bmu.Lock()
	defer r.bmu.Unlock()
	// normal write
	return r.buf.Write(b)
}

func (r *ResliceWriter) ReadFrom(i io.Reader) (int64, error) {
	if r.err != nil {
		return 0, r.err
	}
	nn := int64(0)
	m := 0
	for {
		b := make([]byte, r.c.MSS)
		m, r.err = i.Read(b)
		if r.err != nil {
			if errors.Is(r.err, io.EOF) {
				r.err = nil
			}
			return nn, r.err
		}
		if m < 0 {
			panic("negative read")
		}

		m, r.err = r.Write(b)
		if r.err != nil {
			return nn, r.err
		}
		nn += int64(m)
	}
}

var ErrWholeExceedMSS = errors.New("nettool: whole frame exceed MSS")

// WriteWhole write data without fragmentation, block until data has been sent.
// NOTE: discrete frame for data is not guaranteed, may send with padding and other data
func (r *ResliceWriter) WriteWhole(b []byte) (int, error) {
	if r.err != nil {
		return 0, r.err
	}
	if len(b) > r.c.MSS {
		r.err = ErrWholeExceedMSS
		return 0, ErrWholeExceedMSS
	}
	// ensure only 1 function running
	r.wfmu.Lock()
	defer r.wfmu.Unlock()

	// write to buffer for send
	r.wf = b
	// doWrite will write to wfch when done
	<-r.wfch
	return len(b), nil
}

func (r *ResliceWriter) Close() error {
	r.Flush()
	ret := r.err
	r.err = os.ErrClosed
	return ret
}
