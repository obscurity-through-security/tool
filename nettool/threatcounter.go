package nettool

import (
	"net/netip"
	"time"

	"codeberg.org/obscurity-through-security/tool"
)

type ThreatCounter struct {
	m     tool.RWMap[netip.Prefix, float64]
	total float64
}

// todo possible too much long goroutine

func (t *ThreatCounter) PutWithTime(addr netip.Addr, score float64, tm time.Duration) {
	t.Put(addr, score)

	go func() {
		time.Sleep(tm)
		for i := 0; i < 10; i++ {
			time.Sleep(tm)
			// 1/2+1/4+1/8+...+1/1024+1/1024=1
			score /= 2
			t.Put(addr, -score)
		}
		t.Put(addr, -score)
	}()
}

func (t *ThreatCounter) Put(addr netip.Addr, score float64) {
	addr = addr.WithZone("").Unmap()
	lpfx := addr.BitLen()
	for lpfx > 0 {
		p, _ := addr.Prefix(lpfx)
		t.m.Put(p, score)

		if lpfx > 64 {
			lpfx -= 64
		} else if lpfx > 32 {
			lpfx -= 8
			score /= 2
		} else {
			lpfx -= 4
			score /= 2
		}
	}
	t.total += score
}

func (t *ThreatCounter) Get(addr netip.Addr) float64 {
	addr = addr.WithZone("").Unmap()
	lpfx := addr.BitLen()
	score := 0.0
	for lpfx > 0 {
		p, _ := addr.Prefix(lpfx)

		s, ok := t.m.Get(p)
		if ok {
			score += s
		}

		if lpfx > 64 {
			lpfx -= 64
		} else if lpfx > 32 {
			lpfx -= 8
		} else {
			lpfx -= 4
		}
	}
	return score
}
