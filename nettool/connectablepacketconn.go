package nettool

import (
	"errors"
	"net"

	"codeberg.org/obscurity-through-security/tool"
)

type ConnectablePacketConn struct {
	net.PacketConn
	raddr net.Addr
}

func (c *ConnectablePacketConn) Read(b []byte) (int, error) {
	for {
		n, a, e := c.ReadFrom(b)
		if e != nil {
			return n, e
		}
		if tool.Null(c.raddr) {
			c.raddr = a
			return n, e
		}
		if a.String() == c.raddr.String() {
			return n, e
		}
	}
}

func (c *ConnectablePacketConn) Write(b []byte) (int, error) {
	if tool.Null(c.raddr) {
		return 0, errors.New("not connected yet")
	}
	return c.WriteTo(b, c.raddr)
}

func (c *ConnectablePacketConn) RemoteAddr() net.Addr {
	return c.raddr
}

func (c *ConnectablePacketConn) Connect(a net.Addr) {
	c.raddr = a
}
