package nettool

import (
	"errors"
	"fmt"
	"net"
	"sync"
	"time"

	"codeberg.org/obscurity-through-security/tool"
)

type PersistentConn struct {
	c             net.Conn
	mu            sync.RWMutex
	RetryTime     int
	RetryDelay    time.Duration
	ExponentDelay bool
	ConnectFunc   func() (net.Conn, error)

	writewg   sync.WaitGroup
	lazystart bool
}

var _ net.Conn = &PersistentConn{}

func NewPersistentConn(f func() (net.Conn, error)) *PersistentConn {
	ret := &PersistentConn{
		ConnectFunc: f,
		RetryDelay:  1 * time.Second,
	}
	return ret
}

var ErrTooMuchRetry = errors.New("nettool: not enough retry count")

func (p *PersistentConn) Connect(n int) error {
	var e error
	for i := 0; i < 5; i++ {
		if err := p.renew(); err != nil {
			if errors.Is(err, ErrTooMuchRetry) {
				return err
			}
			e = err
		} else {
			return nil
		}
	}
	return e
}

func (p *PersistentConn) LazyStart() {
	if !p.lazystart {
		p.writewg.Add(1)
		p.lazystart = true
	}
}

func (p *PersistentConn) clearLazyStart() {
	if p.lazystart {
		p.lazystart = false
		p.writewg.Done()
	}
}

func (p *PersistentConn) ensureExist() error {
	if tool.Null(p.c) {
		return p.renew()
	}
	return nil
}

func (p *PersistentConn) renew() error {
	p.mu.Lock()
	defer p.mu.Unlock()
	p.clearLazyStart()
	if p.c != nil {
		p.c.Close()
	}
	if p.RetryTime == -1 {
		return ErrTooMuchRetry
	}
	time.Sleep(p.RetryDelay)
	if p.RetryTime != 0 {
		p.RetryTime--
	}
	if p.RetryTime == 0 {
		p.RetryTime = -1
	}
	if p.ExponentDelay {
		p.RetryDelay *= 2
	}
	c, err := p.ConnectFunc()
	if err != nil {
		return err
	}
	p.c = c
	return nil
}

const cantRenewMessage = "nettool: restart connection failed, %v: base error %w"

func (p *PersistentConn) Read(b []byte) (int, error) {
	p.writewg.Wait()
	for {
		p.ensureExist()
		p.mu.RLock()

		n, err := p.c.Read(b)
		p.mu.RUnlock()
		if err == nil {
			return n, err
		}
		if erenew := p.renew(); erenew != nil {
			return 0, fmt.Errorf(cantRenewMessage, erenew, err)
		}
	}
}

func (p *PersistentConn) Write(b []byte) (int, error) {
	for {
		p.ensureExist()
		p.mu.RLock()

		n, err := p.c.Write(b)
		p.mu.RUnlock()
		if err == nil {
			return n, err
		}
		if erenew := p.renew(); erenew != nil {
			return 0, fmt.Errorf(cantRenewMessage, erenew, err)
		}
	}
}

func (p *PersistentConn) Close() error {
	p.mu.Lock()
	defer p.mu.Unlock()
	p.RetryTime = -1
	return p.c.Close()
}

func (p *PersistentConn) NetConn() net.Conn {
	return p.c
}

func (p *PersistentConn) LocalAddr() net.Addr {
	if err := p.ensureExist(); err != nil {
		return nil
	}
	return p.c.LocalAddr()
}

func (p *PersistentConn) RemoteAddr() net.Addr {
	if err := p.ensureExist(); err != nil {
		return nil
	}
	return p.c.RemoteAddr()
}

func (p *PersistentConn) SetDeadline(t time.Time) error {
	if err := p.ensureExist(); err != nil {
		return err
	}
	return p.c.SetDeadline(t)
}

func (p *PersistentConn) SetReadDeadline(t time.Time) error {
	if err := p.ensureExist(); err != nil {
		return err
	}
	return p.c.SetReadDeadline(t)
}

func (p *PersistentConn) SetWriteDeadline(t time.Time) error {
	if err := p.ensureExist(); err != nil {
		return err
	}
	return p.c.SetWriteDeadline(t)
}
