package nettool

import (
	"bytes"
	"encoding/base32"

	"codeberg.org/obscurity-through-security/tool/rng"
)

// todo more realistic server name
// use gfwatch or other data to avoid collision with blocked server name
const freqEncoding = "etaoinshrdlucmwfgypbvk0123456789" // no jxqz
func RandomHostname() string {
	l := rng.Intn(16)
	b := rng.Bytes(10)
	out := bytes.Buffer{}
	enc := base32.NewEncoder(base32.NewEncoding(freqEncoding), &out)
	enc.Write(b)
	enc.Close()
	ob := out.Bytes()
	return string(ob[:l])
}
