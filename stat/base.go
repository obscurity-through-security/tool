package stat

import (
	"math"

	"golang.org/x/exp/constraints"
)

type number interface {
	constraints.Integer | constraints.Float
}

func ToFloat64[T number](a []T) []float64 {
	if c, ok := any(a).([]float64); ok {
		return c
	}
	b := make([]float64, len(a))
	for i := 0; i < len(a); i++ {
		b[i] = any(a[i]).(float64)
	}
	return b
}

func Average(a []float64) float64 {
	return Sum(a) / float64(len(a))
}

func Sum(a []float64) float64 {
	s := 0.0
	for _, v := range a {
		s += v
	}
	return s
}

func StandardDeviation(a []float64) float64 {
	s2 := 0.0
	s := 0.0
	for _, v := range a {
		s += v
		s2 += v * v
	}
	return math.Sqrt(s2 - s*s)
}
