package tool

import (
	"io"
	"reflect"
)

func WriteFull(w io.Writer, b []byte) error {
	n, err := w.Write(b)
	if err != nil {
		return err
	}
	if n != len(b) {
		return io.ErrShortWrite
	}
	return nil
}

func Null(v any) bool {
	return v == nil || reflect.ValueOf(v).IsNil()
}
