package tool

import "sync"

type RWMap[K comparable, V any] struct {
	p       map[K]V
	mu      sync.RWMutex
	ndelete int
	ninsert int

	shrinkLimit int
}

func NewRWMap[K comparable, V any]() *RWMap[K, V] {
	m := &RWMap[K, V]{}
	m.Clear()
	m.SetShrinkLimit(1e6)
	return m
}

func (r *RWMap[K, V]) Clear() {
	r.mu.Lock()
	defer r.mu.Unlock()
	r.changeMap(map[K]V{})
}

func (r *RWMap[K, V]) changeMap(m map[K]V) {
	r.p = m
	r.ndelete = 0
	r.ninsert = len(m)
}

func (r *RWMap[K, V]) Each(f func(K, V)) {
	r.mu.RLock()
	defer r.mu.RUnlock()
	for k, v := range r.p {
		f(k, v)
	}
}

func (r *RWMap[K, V]) Get(k K) (V, bool) {
	r.mu.RLock()
	defer r.mu.RUnlock()
	v, b := r.p[k]
	return v, b
}

func (r *RWMap[K, V]) Put(k K, v V) {
	r.mu.Lock()
	defer r.mu.Unlock()
	r.p[k] = v
	r.ninsert++
}

func (r *RWMap[K, V]) Remove(k K) {
	r.mu.Lock()
	defer r.mu.Unlock()
	delete(r.p, k)
	r.ndelete++

	remain := r.ninsert - r.ndelete
	if r.ndelete*3 > remain*2 || r.ndelete > r.shrinkLimit {
		r.locklessShrink()
	}
}

func (r *RWMap[K, V]) Shrink() {
	r.mu.Lock()
	defer r.mu.Unlock()
	r.locklessShrink()
}

func (r *RWMap[K, V]) SetShrinkLimit(l int) {
	r.shrinkLimit = l
	if r.ndelete > r.shrinkLimit {
		r.mu.Lock()
		defer r.mu.Unlock()
		r.locklessShrink()
	}
}

func (r *RWMap[K, V]) locklessShrink() {
	t := make(map[K]V, len(r.p))
	for k, v := range r.p {
		t[k] = v
	}
	r.changeMap(t)
}

func (r *RWMap[K, V]) Size() int {
	return len(r.p)
}
